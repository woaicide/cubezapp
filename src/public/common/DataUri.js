class DataUri {
    static MainFrame_ShowWindow = 'DataUri_MainFrame_ShowWindow';
    static App_CloseAllWindow = 'DataUri_App_CloseAllWindow';
    static SettingFrame_SetTaskbarIcon = 'DataUri_SettingFrame_SetTaskbarIcon';
    static APP_PopupMenuClick = 'DataUri_APP_PopupMenuClick';
    static APP_PopupMenu = 'DataUri_APP_PopupMenu';
    static APP_LanguageChange = 'DataUri_APP_LanguageChange';
    static SettingFrame_ShowWindow = 'DataUri_SettingFrame_ShowWindow';
    static MainFrame_ResetWindow = 'DataUri_MainFrame_ResetWindow';
    static CalendarFrame_ShowWindow = 'DataUri_CalendarFrame_ShowWindow';
    static ChatFrame_ShowWindow = 'DataUri_ChatFrame_ShowWindow';
    static ChatFrame_SetData = 'DataUri_ChatFrame_SetData';
    static ChatFrame_OnMessage = 'DataUri_ChatFrame_OnMessage';
    static ChatFrame_OnTransFileReq = 'DataUri_ChatFrame_OnTransFileReq';
    static ChatFrame_ItemClose = 'DataUri_ChatFrame_ItemClose';
    static AboutFrame_ShowWindow = 'DataUri_AboutFrame_ShowWindow';
    static APP_NotePopupMenuClick = 'DataUri_APP_NotePopupMenuClick';
    static APP_NotePopupMenu = 'DataUri_APP_NotePopupMenu';
    static NoteFrame_ShowWindow = 'DataUri_NoteFrame_ShowWindow';
    static NoteFrame_SetData = 'DataUri_NoteFrame_SetData';
    static NoteFrame_CloseWindow = 'DataUri_NoteFrame_CloseWindow';
    static NoteFrame_HideWindow = 'DataUri_NoteFrame_HideWindow';
    static NoteFrame_DoCloseWindow = 'DataUri_NoteFrame_DoCloseWindow';
    static NoteFrame_SaveData = 'DataUri_NoteFrame_SaveData';
    static NoteFrame_AlwaysOnTop = 'DataUri_NoteFrame_AlwaysOnTop';
    static NoteFrame_SetAlarmTime = 'DataUri_NoteFrame_SetAlarmTime';
    static NoteFrame_NotifyClockAlarm = 'DataUri_NoteFrame_NotifyClockAlarm';
    static NoteFrame_CancelAlwaysOnTop = 'DataUri_NoteFrame_CancelAlwaysOnTop';
}

export const DataUriPlugin = {
  install (Vue) {
    Vue.prototype.$DataUri = DataUri
  }
}

export {
  DataUri as default
}
